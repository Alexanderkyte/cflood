CFLAGS=-std=c99 -Wall -pthread -ggdb

cflood:
	gcc cflood.c $(CFLAGS) -o cflood	

clean:
	rm ./cflood
